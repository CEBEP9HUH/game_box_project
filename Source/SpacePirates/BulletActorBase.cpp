// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Components/StaticMeshComponent.h"

#include "BulletActorBase.h"

// Sets default values
ABulletActorBase::ABulletActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BulletStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletStaticMesh"));
	MoveSpeed = 20.f;
}

// Called when the game starts or when spawned
void ABulletActorBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABulletActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

// Move
void ABulletActorBase::Move()
{
	FVector CurPoint = GetActorLocation();
	if(offset.Size() == 0)
	{
		FVector Path = direction - CurPoint;
		float L = Path.Size();
		float x = MoveSpeed * (direction.X - CurPoint.X) / L;
		float y = MoveSpeed * (direction.Y - CurPoint.Y) / L;
		offset.Set(x, y, 0); 
	}
	AddActorWorldOffset(offset);
}

