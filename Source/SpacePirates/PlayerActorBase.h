// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceShipActorBase.h"
#include "PlayerActorBase.generated.h"

UCLASS()
class SPACEPIRATES_API APlayerActorBase : public ASpaceShipActorBase
{
	GENERATED_BODY()

	// Time from last extra Ammo getting
	float LastExtraAmmoTime;
	
public:	
	// Sets default values for this actor's properties
	APlayerActorBase();

	// Time to get extra Ammo
	UPROPERTY(EditDefaultsOnly)
		float ExtraAmmoTime;

	// Sound, played at getting extra ammo
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UAudioComponent* ExtraAmmoSound;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Get additional Ammo
	void IncreaseAmmo(const int count = 1) override final;
};
