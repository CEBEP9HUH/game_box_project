// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"

#include "EnemySpawnActorBase.h"
#include "EnemyActorBase.h"

// Sets default values
AEnemySpawnActorBase::AEnemySpawnActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	EnemySpawnStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EnemySpawnStaticMesh"));
	TimeFromLastSpawn = 0.f;
	DefaultEnemyAmmo = 0;
	SpawnAngleInterval = 90;
}

// Called when the game starts or when spawned
void AEnemySpawnActorBase::BeginPlay()
{
	Super::BeginPlay();
	UpdateSumOfChances();
}

// Called every frame
void AEnemySpawnActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Spawn enemy ship
void AEnemySpawnActorBase::SpawnEnemy()
{
	TimeFromLastSpawn = 0;
	FRotator rotation = GetActorRotation();
	FVector scale = RootComponent->GetComponentScale()*50;
	scale = scale.RotateAngleAxis(rotation.Yaw, FVector(0, 0, 1));
	FVector spawn_point = UKismetMathLibrary::RandomPointInBoundingBox(GetActorLocation(), scale);
	rotation.Yaw += UKismetMathLibrary::RandomFloatInRange(-SpawnAngleInterval/2, SpawnAngleInterval/2);
	float random_ship = UKismetMathLibrary::RandomFloatInRange(0, SumOfChances);
	float popularity = 0;
	for (const auto& ship : UEnemyActorBase)
	{
		popularity += ship.Value;
		if (popularity >= random_ship)
		{
			spawn_point.Z = 0;
			auto enemy = GetWorld()->SpawnActor<AEnemyActorBase>(ship.Key, spawn_point, rotation);
			if(IsValid(enemy))
				enemy->Ammo = DefaultEnemyAmmo;
			break;
		}
	}
}

void AEnemySpawnActorBase::UpdateSumOfChances()
{
	SumOfChances = 0.f;
	for (const auto& ship : UEnemyActorBase)
		SumOfChances += ship.Value;
}

