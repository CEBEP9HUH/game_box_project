// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BulletActorBase.generated.h"

UCLASS()
class SPACEPIRATES_API ABulletActorBase : public AActor
{
	GENERATED_BODY()

private:
	// Direction * speed
	FVector offset;
	
public:	
	// Sets default values for this actor's properties
	ABulletActorBase();

	// Static mesh of bullet
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* BulletStaticMesh;

	//Movement speed
	UPROPERTY(EditDefaultsOnly)
		float MoveSpeed;

	// Move direction
	FVector direction;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Move
	void Move();
};
