// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyActorBase.h"

// Sets default values
AEnemyActorBase::AEnemyActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DropChance = 0.3f;
	ReloadTime = 1.f;
	Reloading = 0.f;
}

// Called when the game starts or when spawned
void AEnemyActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	if (Reloading < ReloadTime)
		Reloading += DeltaTime;
	else
	{
		Reloading = 0;
		Fire();
	}
}

//Move
void AEnemyActorBase::Move()
{
	MoveParameters();
	FVector CurPoint = GetActorLocation();
	FVector Destination = GetActorForwardVector();
	Destination *= MoveSpeed;
	FVector WorldOfset(Destination);
	AddActorWorldOffset(WorldOfset);
}

// Movement parameters (trajectory etc)
void AEnemyActorBase::MoveParameters_Implementation()
{

}


