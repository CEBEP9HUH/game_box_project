// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceShipActorBase.h"
#include "EnemyActorBase.generated.h"

UCLASS()
class SPACEPIRATES_API AEnemyActorBase : public ASpaceShipActorBase
{
	GENERATED_BODY()

	// Time from last shot
	float Reloading;
	
public:	
	// Sets default values for this actor's properties
	AEnemyActorBase();

	// Chance to drop bonus after death
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DropChance;

	// Time between shots
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float ReloadTime;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Move
	void Move();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Movement parameters (trajectory etc)
	UFUNCTION(BlueprintNativeEvent)
		void MoveParameters();
		void MoveParameters_Implementation();

};
