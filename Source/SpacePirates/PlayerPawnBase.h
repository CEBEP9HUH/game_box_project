// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class APlayerActorBase;

class UCameraComponent;
class UPlayerController;

UCLASS()
class SPACEPIRATES_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

private:
	// Player controller
	APlayerController* PlayerController;
	// If true - player actor move to cursor
	bool NeedToMove;
	// Datas for convertion from screen coords to world coords
	FVector2D ViewportSize;

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	// Player's actor
	UPROPERTY(BlueprintReadWrite)
		APlayerActorBase* PlayerActor;

	// Player's actor type
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<APlayerActorBase> UPlayerActorBase;

	// Player's camera
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCameraComponent* PlayerCamera;

	// Movement speed
	UPROPERTY(EditDefaultsOnly)
		float MoveSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	// Move player ship to mouse cursor
	void Move();
	// Fire
	void Fire();
	// Rotate players ship to mouse cursor
	void Rotate();
	// Allow to move
	void StartMove();
	// Ban to move
	void StopMove();
	// Get mouse location in world coords
	FVector GetMouseLocationFromZeros();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
