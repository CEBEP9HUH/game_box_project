// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpaceShipActorBase.generated.h"

class ABulletActorBase;

class UAudioComponent;
class UStaticMeshComponent;

UCLASS()
class SPACEPIRATES_API ASpaceShipActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpaceShipActorBase();

	//Static mesh of enemy ship
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* ShipStaticMesh;

	// Bullet's type
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABulletActorBase> UBulletActorBase;

	// Sound, played at fire
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UAudioComponent* FireSound;

	// Ammo count
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int Ammo;

	// MovementSpeed
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float MoveSpeed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Get additional Ammo
	UFUNCTION(BlueprintCallable)
		virtual void IncreaseAmmo(const int count = 1);

	// Fire
	virtual void Fire();

};
