// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Components/AudioComponent.h"

#include "PlayerActorBase.h"

// Sets default values
APlayerActorBase::APlayerActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ExtraAmmoSound = CreateDefaultSubobject<UAudioComponent>(TEXT("ExtraAmmoSound"));
	ExtraAmmoTime = 1.f;
	LastExtraAmmoTime = 0.f;
}

// Called when the game starts or when spawned
void APlayerActorBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (ExtraAmmoTime >= LastExtraAmmoTime)
		LastExtraAmmoTime += DeltaTime;
	else
	{
		IncreaseAmmo();
		LastExtraAmmoTime = 0;
	}
}

// Get additional Ammo
void APlayerActorBase::IncreaseAmmo(const int count)
{
	ASpaceShipActorBase::IncreaseAmmo(count);
	ExtraAmmoSound->Play();
}

