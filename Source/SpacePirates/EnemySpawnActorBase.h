// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnActorBase.generated.h"

class AEnemyActorBase;

class UStaticMeshComponent;

UCLASS()
class SPACEPIRATES_API AEnemySpawnActorBase : public AActor
{
	GENERATED_BODY()

private:
	// Time from last enemy ship spawn
	float TimeFromLastSpawn;

	// Sum of all chances to spawn any ship
	float SumOfChances;
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawnActorBase();
	
	// Enemy type and spawn chance
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TMap<TSubclassOf<AEnemyActorBase>, float  > UEnemyActorBase;

	// Static mesh for spawn point
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* EnemySpawnStaticMesh;

	// Interval of spawning [-SpawnAngleInterval/2, SpawnAngleInterval/2]
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SpawnAngleInterval;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int DefaultEnemyAmmo;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Spawn enemy ship
	UFUNCTION(BlueprintCallable)
		void SpawnEnemy();

	// Update sum of all chances to spawn any ship
	UFUNCTION(BlueprintCallable)
		void UpdateSumOfChances();
};
