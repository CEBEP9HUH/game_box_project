// Fill out your copyright notice in the Description page of Project Settings.

#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/GameFramework/PlayerController.h"

#include "PlayerPawnBase.h"
#include "PlayerActorBase.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	RootComponent = PlayerCamera;
	NeedToMove = false;
	MoveSpeed = 0.f;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(-90, -90, 0));
	PlayerActor = GetWorld()->SpawnActor<APlayerActorBase>(UPlayerActorBase, FTransform());
	PlayerController = Cast<APlayerController>(GetController());
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(IsValid(PlayerActor))
	{ 
		Rotate();
		Move();
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Move", IE_Pressed, this, &APlayerPawnBase::StartMove);
	PlayerInputComponent->BindAction("Move", IE_Released, this, &APlayerPawnBase::StopMove);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerPawnBase::Fire);
}

// Rotate players ship to mouse cursor
void APlayerPawnBase::Rotate()
{
	FVector DestDirection = GetMouseLocationFromZeros();
	FVector CurDirection = PlayerActor->GetActorLocation();
	FRotator rotation_offset = UKismetMathLibrary::FindLookAtRotation(CurDirection, DestDirection);
	PlayerActor->SetActorRotation(rotation_offset);
}

// Move player ship to mouse cursor
void APlayerPawnBase::Move()
{
	if (NeedToMove)
	{
		FVector DestPoint = GetMouseLocationFromZeros();
		FVector CurPoint = PlayerActor->GetActorLocation();
		FVector Path = DestPoint - CurPoint;
		float L = Path.Size();
		if (L > 100)
		{
			PlayerActor->AddActorWorldOffset(Path/L*MoveSpeed);
		}
	}
}

// Fire
void APlayerPawnBase::Fire()
{
	if (IsValid(PlayerActor))
	{
		PlayerActor->Fire();
	}
}

// Allow to move
void APlayerPawnBase::StartMove()
{
	NeedToMove = true;
}

// Ban to move
void APlayerPawnBase::StopMove()
{
	NeedToMove = false;
}

// Get mouse location in world coords
FVector APlayerPawnBase::GetMouseLocationFromZeros()
{
	FVector WorldMouseLocation(0, 0, 0);
	FVector WorldMouseDirection(0, 0, 0);
	if (IsValid(PlayerController))
	{
		PlayerController->DeprojectMousePositionToWorld(WorldMouseLocation, WorldMouseDirection);
	}
	return FVector(WorldMouseLocation.X * GetActorLocation().Z / 10, WorldMouseLocation.Y * GetActorLocation().Z / 10, 0);
}

