// Fill out your copyright notice in the Description page of Project Settings.


#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Components/AudioComponent.h"

#include "BulletActorBase.h"
#include "SpaceShipActorBase.h"

// Sets default values
ASpaceShipActorBase::ASpaceShipActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	ShipStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipStaticMesh"));
	FireSound = CreateDefaultSubobject<UAudioComponent>(TEXT("FireSound"));
	MoveSpeed = 10.f;
	Ammo = 0; 
}

// Called when the game starts or when spawned
void ASpaceShipActorBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpaceShipActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASpaceShipActorBase::Fire()
{
	if (Ammo > 0)
	{
		FireSound->Play();
		auto bullet_spawn_point = ShipStaticMesh->GetSocketLocation("bullet_spawn_point");
		auto bullet_direction = ShipStaticMesh->GetSocketLocation("bullet_direction");
		auto bullet = GetWorld()->SpawnActor<ABulletActorBase>(UBulletActorBase, FTransform(bullet_spawn_point));
		if (IsValid(bullet))
			bullet->direction = bullet_direction;
		Ammo--;
	}
}

// Get additional Ammo
void ASpaceShipActorBase::IncreaseAmmo(const int count)
{
	Ammo += count;
}

